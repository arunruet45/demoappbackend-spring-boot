package com.demowellstack.demowellstack.controller;

import com.demowellstack.demowellstack.AllResponse.Response;
import com.demowellstack.demowellstack.AllResponse.ResponseType.AuthenticationSuccess;
import com.demowellstack.demowellstack.AllResponse.ResponseType.DataExist;
import com.demowellstack.demowellstack.AllResponse.ResponseType.DataNotFound;
import com.demowellstack.demowellstack.dao.*;
import com.demowellstack.demowellstack.entity.*;
import com.demowellstack.demowellstack.AllResponse.UserException;
import com.demowellstack.demowellstack.service.*;
import com.fasterxml.jackson.databind.jsontype.impl.AsWrapperTypeDeserializer;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/users/")
public class UserController {

    @Autowired
    UserDao userDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    UserException userException;

    @Autowired
    Response response;

    @Autowired
    AnswerDao answerdao;

    @Autowired
    LikeDislikeDao likeDislikeDao;

    @Autowired
    CategoryCrud categoryCrud;

    @Autowired
    QuestionCrud questionCrud;

    @Autowired
    AnswerCrud answerCrud;

    @Autowired
    LikeDislikeCrud likeDislikeCrud;

    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    UserPresentCheck userPresentCheck;

    @Autowired
    UserChecking userChecking;

    @Autowired
    UserMatchChecking userMatchChecking;

    @Autowired
    Adminchecking adminchecking;

    @PostMapping("signUp")
    public ResponseEntity<Object> userSignUp(@RequestBody User user, HttpServletRequest req, HttpServletResponse resp) {
        User reqUser = userDao.findByEmail(user.getEmail());
        if (Objects.nonNull(reqUser)){
            DataExist dataExist = response.dataExisted();
            dataExist.setStatus(226);
            dataExist.setType("Conflict");
            dataExist.setMessage("Email Already Exist");
            return new ResponseEntity<> (dataExist, HttpStatus.IM_USED);
        }
        else {
            User newUser = new User();
            newUser.setEmail(user.getEmail());
            newUser.setPassword(user.getPassword());
            newUser.setType("user");
            userDao.save(newUser);
            HttpSession session = req.getSession();
            UserSession dbSession = new UserSession();
            dbSession.setEmail(user.getEmail());
            dbSession.setValue(session.getId());
            dbSession.setValid(true);
            userSessionRepository.save(dbSession);
            response.LoginSuccess().setSessionId(req.getSession().getId());
            AuthenticationSuccess authenticationSuccess = response.LoginSuccess();
            authenticationSuccess.setSessionId(session.getId());
            authenticationSuccess.setMessage("Sign up Successful");
            return new ResponseEntity<> (authenticationSuccess, HttpStatus.OK);
        }
    }

//    @PostMapping("login")
//    public ResponseEntity<Object> userLogin(@ModelAttribute("user") User user, HttpServletRequest req, HttpServletResponse resp) {
//        User reqUser = userDao.findByEmail(user.getEmail());
//        if(Objects.nonNull(reqUser)){
//            if(reqUser.getPassword().equals(user.getPassword())){
//                if (userChecking.isUserValid(req.getHeader("sessionId")) || userSessionRepository.findByEmailAndValidIsTrue(user.getEmail()).isPresent()){
//                    AuthenticationSuccess authenticationSuccess = new AuthenticationSuccess();
//                    authenticationSuccess.setSessionId(req.getHeader("sessionId"));
//                    authenticationSuccess.setMessage("Already Logged in..!");
//                    return new ResponseEntity<> (authenticationSuccess, HttpStatus.OK);
//                }
//                else {
//                    HttpSession session = req.getSession();
//                    UserSession dbSession = new UserSession();
//                    dbSession.setEmail(user.getEmail());
//                    dbSession.setValue(session.getId());
//                    dbSession.setValid(true);
//                    userSessionRepository.save(dbSession);
//                    if (userDao.findByEmail(user.getEmail()).getType().equals("admin")){
//                        user.setType("admin");
//                        return new ResponseEntity<> (user, HttpStatus.OK);
//                    }
//                    else {
//                        user.setType("user");
//                        return new ResponseEntity<> (user, HttpStatus.OK);
//                    }
//                }
//
//            }
//            else {
//                return new ResponseEntity<> (response.passwordError(), HttpStatus.UNAUTHORIZED);
//            }
//        }
//        else {
//            return new ResponseEntity<> (response.emailError(), HttpStatus.UNAUTHORIZED);
//        }
//    }

    @PostMapping("login")
    public ResponseEntity<Object> userLogin(@RequestBody User user, HttpServletRequest req, HttpServletResponse resp) {
        User reqUser = userDao.findByEmail(user.getEmail());
        if(Objects.nonNull(reqUser)){
            if(reqUser.getPassword().equals(user.getPassword())){
                if (userChecking.isUserValid(req.getHeader("sessionId")) || userSessionRepository.findByEmailAndValidIsTrue(user.getEmail()).isPresent()){
                    AuthenticationSuccess authenticationSuccess = new AuthenticationSuccess();
                    authenticationSuccess.setStatus(200);
                    authenticationSuccess.setType("Success");
                    authenticationSuccess.setSessionId(req.getHeader("sessionId"));
                    authenticationSuccess.setMessage("Already Logged in..!");
                    return new ResponseEntity<> (authenticationSuccess, HttpStatus.OK);

                }
                else {
                    HttpSession session = req.getSession();
                    UserSession dbSession = new UserSession();
                    dbSession.setEmail(user.getEmail());
                    dbSession.setValue(session.getId());
                    dbSession.setValid(true);
                    userSessionRepository.save(dbSession);
                    AuthenticationSuccess authenticationSuccess = response.LoginSuccess();
                    authenticationSuccess.setSessionId(session.getId());
                    return new ResponseEntity<> (authenticationSuccess, HttpStatus.OK);
                }

            }
            else {
                return new ResponseEntity<> (response.passwordError(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<> (response.emailError(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("info")
    public ResponseEntity<Object> userInfo(HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            String userEmail = userSessionRepository.findByValue(req.getHeader("sessionId")).getEmail();
            User user = userDao.findByEmail(userEmail);
            return new ResponseEntity<> (user, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("logOut")
    public ResponseEntity<Object> userLogOut(HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            UserSession userSession = userSessionRepository.findByValue(req.getHeader("sessionId"));
            userSession.setValid(false);
            userSessionRepository.save(userSession);
            req.getSession().invalidate();

            return new ResponseEntity<> (response.loggedOut(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("questions/add")
    public ResponseEntity<Object> addQuestion(@RequestBody Question question, @RequestParam("categoryId") String cId, HttpServletRequest req, HttpServletResponse resp) {
        Integer id = Integer.parseInt(cId);
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (categoryDao.findById(id).isPresent()){
                questionCrud.save(question, id, userSessionRepository.findByValue(req.getHeader("sessionId")).getEmail());
                return new ResponseEntity<> (response.dataAdded(), HttpStatus.OK);
            }
            else {
                DataNotFound dataNotFound = new DataNotFound();
                dataNotFound.setStatus(404);
                dataNotFound.setType("Not Found");
                dataNotFound.setMessage("Category Not found");
                return new ResponseEntity<>(dataNotFound, HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("questions/update/{id}")
    public ResponseEntity<Object> updateQuestion(@RequestBody Question question, @PathVariable int id, @RequestParam("categoryId") int cId, HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (questionDao.findById(id).isPresent()  && userMatchChecking.isQuestionUserMatched(id, req.getHeader("sessionId"))){
                if (categoryDao.findById(cId).isPresent()){
                    questionCrud.update(question, id, cId);
                    return new ResponseEntity<> (response.dataUpdated(), HttpStatus.OK);
                }
                else {
                    DataNotFound dataNotFound = new DataNotFound();
                    dataNotFound.setStatus(404);
                    dataNotFound.setType("Not Found");
                    dataNotFound.setMessage("Category Not found");
                    return new ResponseEntity<>(dataNotFound, HttpStatus.NOT_FOUND);
                }
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("questions/delete/{id}")
    public ResponseEntity<Object> deleteQuestion(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (questionDao.findById(id).isPresent()  && (userMatchChecking.isQuestionUserMatched(id, req.getHeader("sessionId")) || adminchecking.isAdminValid(req.getHeader("sessionId")))){
                questionCrud.delete(id);
                return new ResponseEntity<> (response.dataDeleted(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("{id}/questions/findAll")
    public ResponseEntity<Object> specificUserQuestions(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (userDao.findById(id).isPresent()){
                // List<Question> questions = userDao.findById(id).get().getQuestions();
                List<Question> questions = questionDao.findByUser(userDao.findById(id).get()).get();
                return new ResponseEntity<>(questions,HttpStatus.OK);
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("questions/{id}/answers/findAll")
    public ResponseEntity<Object> specificQuestionAnswer(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            Optional<Question> question = questionDao.findById(id);
            if (question.isPresent()){
                //List<Answer> answers = question.get().getAnswers();
                Optional<List<Answer>> answers = answerdao.findByQuestion(questionDao.findById(id).get());
                if (answers.isPresent()){
                    List<Answer> qAnswers = answers.get();
                    return new ResponseEntity<>(qAnswers,HttpStatus.OK);
                }
                else {
                    DataNotFound dataNotFound = new DataNotFound();
                    dataNotFound.setStatus(404);
                    dataNotFound.setType("Not Found");
                    dataNotFound.setMessage("Answer Not found");
                    return new ResponseEntity<>(dataNotFound, HttpStatus.NOT_FOUND);
                }

            }
            else {
                DataNotFound dataNotFound = new DataNotFound();
                dataNotFound.setStatus(404);
                dataNotFound.setType("Not Found");
                dataNotFound.setMessage("Question Not found");
                return new ResponseEntity<>(dataNotFound, HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("questions/findAll")
    public ResponseEntity<Object> AllQuestions(HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            List<Question> questions = questionDao.findAll();
            return new ResponseEntity<>(questions,HttpStatus.OK);
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("answers/add/{questionId}")
    public ResponseEntity<Object> addAnswer(@RequestBody Answer answer, @PathVariable("questionId") int id, HttpServletRequest req, HttpServletResponse resp) {
      if (userChecking.isUserValid(req.getHeader("sessionId"))) {
          if (questionDao.findById(id).isPresent()){
              answerCrud.save(answer,id, userSessionRepository.findByValue(req.getHeader("sessionId")).getEmail());
              return new ResponseEntity<> (response.dataAdded(), HttpStatus.OK);
          }
          else {
              DataNotFound dataNotFound = new DataNotFound();
              dataNotFound.setStatus(404);
              dataNotFound.setType("Not Found");
              dataNotFound.setMessage("Question Not found");
              return new ResponseEntity<> (dataNotFound, HttpStatus.NOT_FOUND);
          }
      }
        else {
          return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("answers/update/{id}")
    public ResponseEntity<Object> updateAnswer(@RequestBody Answer answer, @PathVariable int id, HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (answerdao.findById(id).isPresent()  && userMatchChecking.isAnswerUserMatched(id, req.getHeader("sessionId"))){
                answerCrud.update(answer, id);
                return new ResponseEntity<> (response.dataUpdated(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("answers/delete/{id}")
    public ResponseEntity<Object> deleteAnswer(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (answerdao.findById(id).isPresent()  && (userMatchChecking.isAnswerUserMatched(id, req.getHeader("sessionId")) || adminchecking.isAdminValid(req.getHeader("sessionId")))){
                answerCrud.delete(id);
                return new ResponseEntity<> (response.dataDeleted(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("answers/like/{answerId}")
    public ResponseEntity<Object> likeAnswer(@PathVariable("answerId") int id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (answerdao.findById(id).isPresent()){
                String likeResponse = likeDislikeCrud.likeSave(id, userSessionRepository.findByValue(req.getHeader("sessionId")).getEmail());
                if (likeResponse.equals("added")){
                    return new ResponseEntity<> (response.dataAdded(), HttpStatus.OK);
                }
                else {
                    return new ResponseEntity<> (response.dataExisted(), HttpStatus.CONFLICT);
                }
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }
    @PostMapping("answers/dislike/{answerId}")
    public ResponseEntity<Object> dislikeAnswer(@PathVariable("answerId") Integer id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            if (answerdao.findById(id).isPresent()){
                String dislikeResponse = likeDislikeCrud.dislikeSave(id, userSessionRepository.findByValue(req.getHeader("sessionId")).getEmail());
                if (dislikeResponse.equals("added")){
                    return new ResponseEntity<> (response.dataAdded(), HttpStatus.OK);
                }
                else {
                    return new ResponseEntity<> (response.dataExisted(), HttpStatus.CONFLICT);
                }
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("answers/{id}/totalLike")
    public ResponseEntity<Object> totalLike(@PathVariable Integer id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            Integer totalLike = likeDislikeDao.countByAnswerAndChoiceIsTrue(answerdao.findById(id).get());
            return new ResponseEntity<> (totalLike, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);

        }
    }

    @GetMapping("answers/{id}/totalDislike")
    public ResponseEntity<Object> totalDislike(@PathVariable Integer id, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            Integer totalLike = likeDislikeDao.countByAnswerAndChoiceIsFalse(answerdao.findById(id).get());
            return new ResponseEntity<> (totalLike, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);

        }
    }

    @GetMapping("categories/findAll")
    public ResponseEntity<Object> findAllCategory(HttpServletRequest req, HttpServletResponse resp) {
        if (userChecking.isUserValid(req.getHeader("sessionId"))){
            List<Category> categories = categoryCrud.findAll();
            return new ResponseEntity<>(categories,HttpStatus.OK);
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }
}
