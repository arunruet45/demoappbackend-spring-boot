package com.demowellstack.demowellstack.controller;

import com.demowellstack.demowellstack.AllResponse.ResponseType.DataExist;
import com.demowellstack.demowellstack.dao.*;
import com.demowellstack.demowellstack.entity.*;
import com.demowellstack.demowellstack.AllResponse.Response;
import com.demowellstack.demowellstack.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/admin/")
public class AdminController {

    @Autowired
    UserDao userDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    UserPresentCheck userPresentCheck;

    @Autowired
    UserChecking userChecking;

    @Autowired
    Adminchecking adminchecking;

    @Autowired
    UserMatchChecking userMatchChecking;

    @Autowired
    CategoryCrud categoryCrud;

    @Autowired
    QuestionCrud questionCrud;

    @Autowired
    AnswerCrud answerCrud;

    @Autowired
    Response response;

    @PostMapping("categories/add")
    public ResponseEntity<Object> addCategory(@RequestBody Category category, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (userChecking.isUserValid(req.getHeader("sessionId")) && adminchecking.isAdminValid(req.getHeader("sessionId"))){
            if (categoryDao.findByType(category.getType()).isPresent()){
                //DataExist dataExist = response.dataExisted();
                //mapper.writeValueAsString(response.dataExisted());
                return new ResponseEntity<> (response.dataExisted(), HttpStatus.CONFLICT);
            }
            else {
                categoryCrud.save(category);
                return new ResponseEntity<> (response.dataAdded(), HttpStatus.OK);
            }

        }
        else {
            return new ResponseEntity<Object> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("categories/update/{id}")
    public ResponseEntity<Object> updateCategory(@RequestBody Category category, @PathVariable int id, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (userChecking.isUserValid(req.getHeader("sessionId")) && adminchecking.isAdminValid(req.getHeader("sessionId"))){
            if (categoryDao.findById(id).isPresent()){
                if (categoryDao.findByType(category.getType()).isPresent()){
                    return new ResponseEntity<> (response.dataExisted(), HttpStatus.CONFLICT);
                }
                else {
                    categoryCrud.update(category, id);
                    return new ResponseEntity<> (response.dataUpdated(), HttpStatus.OK);
                }
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("categories/delete/{id}")
    public ResponseEntity<Object> deleteCategory(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (userChecking.isUserValid(req.getHeader("sessionId")) && adminchecking.isAdminValid(req.getHeader("sessionId"))){
            if (categoryDao.findById(id).isPresent()){
                categoryCrud.delete(id);
                return new ResponseEntity<> (response.dataDeleted(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<> (response.dataNotFound(), HttpStatus.NOT_FOUND);
            }

        }
        else {
            return new ResponseEntity<> (response.sessionExpired(), HttpStatus.UNAUTHORIZED);
        }
    }

}
