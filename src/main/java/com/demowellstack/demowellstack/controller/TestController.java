package com.demowellstack.demowellstack.controller;

import com.demowellstack.demowellstack.dao.UserSessionRepository;
import com.demowellstack.demowellstack.entity.Category;
import com.demowellstack.demowellstack.entity.User;
import com.demowellstack.demowellstack.service.UserChecking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class TestController {

    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    UserChecking userChecking;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginPage(Model model, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getRequestedSessionId())){
//            Cookie cookie = new Cookie("JSESSIONID", req.getRequestedSessionId());
//            resp.addCookie(cookie);
            return "redirect:/home";
        }
        else {
            model.addAttribute("user",new User());
            return "login";
        }

    }

    @RequestMapping("/home")
    public String homePage(){
        return "home";
    }

    @RequestMapping("/signup")
    public String signupPage(Model model, HttpServletRequest req, HttpServletResponse resp){
        if (userChecking.isUserValid(req.getRequestedSessionId())){
            return "redirect:/home";
        }
        else{
            model.addAttribute("user",new User());
            return "signup";
        }
    }

    @RequestMapping("/categoryAdd")
    public String categoryAdd(Model model){
        model.addAttribute("category",new Category());
        return "category";
    }
}
