package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.Category;
import com.demowellstack.demowellstack.entity.Question;
import com.demowellstack.demowellstack.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface QuestionDao extends JpaRepository<Question, Integer> {
    Optional<Question> findByDescription(String description);
    Optional<List<Question>> findByCategory(Category category);
    Optional<List<Question>> findByUser(User user);
    void deleteByCategory(Category category);
}
