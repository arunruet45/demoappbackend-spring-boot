package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.Answer;
import com.demowellstack.demowellstack.entity.Question;
import com.demowellstack.demowellstack.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AnswerDao extends JpaRepository<Answer, Integer> {
    Optional<Answer> findByDescription(String description);
    Optional<List<Answer>> findByQuestion(Question question);
}
