package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.Answer;
import com.demowellstack.demowellstack.entity.LikeDislike;
import com.demowellstack.demowellstack.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LikeDislikeDao extends JpaRepository<LikeDislike, Integer> {
    Optional<LikeDislike> findByUserAndChoiceIsTrue(User user);
    Optional<LikeDislike> findByUserAndChoiceIsFalse(User user);
    Integer countByAnswerAndChoiceIsTrue(Answer answer);
    Integer countByAnswerAndChoiceIsFalse(Answer answer);
    Optional<List<LikeDislike>> findByAnswer(Answer answer);
}
