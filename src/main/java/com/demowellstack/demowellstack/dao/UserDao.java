package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {
    User findByEmail(String email);

}
