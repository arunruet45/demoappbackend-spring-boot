package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CategoryDao extends JpaRepository<Category, Integer> {

//    @Modifying
//    @Query("update Category c set c.setType = :type where c.id = :id")
//    public void updateCategory(@Param("type") String type, @Param("id") int id);

    public Optional<Category> findByType(String type);
    public Optional<Category> findByIdAndType(int id, String type);
}
