package com.demowellstack.demowellstack.dao;

import com.demowellstack.demowellstack.entity.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserSessionRepository extends JpaRepository<UserSession, Integer> {
    Optional<UserSession> findByValueAndValidIsTrue(String id);
    UserSession findByValue(String id);
    Optional<UserSession> findByEmailAndValidIsTrue(String email);
}
