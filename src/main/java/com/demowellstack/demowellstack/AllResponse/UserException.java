package com.demowellstack.demowellstack.AllResponse;

import org.springframework.stereotype.Component;

@Component
public class UserException {
    private String email= "Email not Found";
    private String password = "Password Not found";

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = "Email not Found";
    }

    public void setPassword(String password) {
        this.password = "password Not Found";
    }
}
