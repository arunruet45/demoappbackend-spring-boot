package com.demowellstack.demowellstack.AllResponse;

import com.demowellstack.demowellstack.AllResponse.ResponseType.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Response {

    public AuthenticationSuccess LoginSuccess(){
        AuthenticationSuccess authenticationSuccess  = new AuthenticationSuccess();
        return authenticationSuccess;
    }
//    public AuthenticationSuccess previousLoginSuccess(){
//        AuthenticationSuccess authenticationSuccess  = new AuthenticationSuccess();
//        authenticationSuccess.setMessage("Already logged in");
//        authenticationSuccess.setSessionId();
//        return authenticationSuccess;
//    }
    public Logout loggedOut(){
        Logout logout = new Logout();
        return logout;
    }
    public AuthenticationError emailError(){
        AuthenticationError authenticationError = new AuthenticationError();
        authenticationError.setMessage("Email Not Found");
        return authenticationError;
    }
    public AuthenticationError passwordError(){
        AuthenticationError authenticationError = new AuthenticationError();
        authenticationError.setMessage("Password Not Found");
        return authenticationError;
    }

    public DataNotFound dataNotFound(){
        DataNotFound dataNotFound = new DataNotFound();
        return dataNotFound;
    }

    public DataAdd dataAdded(){
        DataAdd dataAdd = new DataAdd();
        return dataAdd;
    }

    public DataDelete dataDeleted(){
        DataDelete dataDelete = new DataDelete();
        return dataDelete;
    }

    public DataExist dataExisted(){
        DataExist dataExist = new DataExist();
        return dataExist;
    }

    public DataUpdate dataUpdated(){
        DataUpdate dataUpdate = new DataUpdate();
        return dataUpdate;
    }

    public SessionExpire sessionExpired(){
        SessionExpire sessionExpire = new SessionExpire();
        return sessionExpire;
    }

}
