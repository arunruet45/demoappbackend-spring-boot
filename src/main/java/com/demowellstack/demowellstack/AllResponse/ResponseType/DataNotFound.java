package com.demowellstack.demowellstack.AllResponse.ResponseType;

public class DataNotFound {
    private Integer status = 404;
    private String type = "Not found";
    private String message = "Data not found for operation";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
