package com.demowellstack.demowellstack.AllResponse.ResponseType;

public class Logout {
    private Integer status = 200;
    private String type = "successful";
    private String message = "LogOut Successful";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
