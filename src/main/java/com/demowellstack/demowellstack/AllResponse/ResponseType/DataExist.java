package com.demowellstack.demowellstack.AllResponse.ResponseType;

public class DataExist {
    private Integer status = 409;
    private String type = "Conflict";
    private String message = "Data already exist";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
