package com.demowellstack.demowellstack.AllResponse.ResponseType;

public class AuthenticationSuccess {
    private Integer status = 200 ;
    private String type = "Success";
    private String sessionId;
    private String message = "Log in Successful";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
