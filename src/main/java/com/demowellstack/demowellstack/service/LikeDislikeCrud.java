package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.AnswerDao;
import com.demowellstack.demowellstack.dao.LikeDislikeDao;
import com.demowellstack.demowellstack.dao.QuestionDao;
import com.demowellstack.demowellstack.dao.UserDao;
import com.demowellstack.demowellstack.entity.LikeDislike;
import com.demowellstack.demowellstack.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LikeDislikeCrud {

    @Autowired
    LikeDislikeDao likeDislikeDao;

    @Autowired
    UserDao userDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    AnswerDao answerDao;

    public String likeSave(int id, String email){
        User user = userDao.findByEmail(email);
        Optional<LikeDislike> likeFalse = likeDislikeDao.findByUserAndChoiceIsFalse(user);
        if (likeFalse.isPresent()){
            LikeDislike like = likeFalse.get();
            like.setAnswer(answerDao.findById(id).get());
            like.setUser(userDao.findByEmail(email));
            like.setChoice(true);
            likeDislikeDao.save(like);
            return "added";
        }
        else{
            Optional<LikeDislike> likeTrue = likeDislikeDao.findByUserAndChoiceIsTrue(user);
            if (likeTrue.isPresent()){
                return "already added";
            }
            else {
                LikeDislike like = new LikeDislike();
                like.setAnswer(answerDao.findById(id).get());
                like.setUser(userDao.findByEmail(email));
                like.setChoice(true);
                likeDislikeDao.save(like);
                return "added";

            }
        }
    }

    public String dislikeSave(int id, String email){
        User user = userDao.findByEmail(email);
        Optional<LikeDislike> likeTrue = likeDislikeDao.findByUserAndChoiceIsTrue(user);
        if (likeTrue.isPresent()){
            LikeDislike dislike = likeTrue.get();
            dislike.setAnswer(answerDao.findById(id).get());
            dislike.setUser(userDao.findByEmail(email));
            dislike.setChoice(false);
            likeDislikeDao.save(dislike);
            return "added";

        }
        else {
            Optional<LikeDislike> likeFalse = likeDislikeDao.findByUserAndChoiceIsFalse(user);
            if (likeFalse.isPresent()){
                return "already added";
            }
            else {
                LikeDislike dislike = new LikeDislike();
                dislike.setAnswer(answerDao.findById(id).get());
                dislike.setUser(userDao.findByEmail(email));
                dislike.setChoice(false);
                likeDislikeDao.save(dislike);
                return "added";
            }

        }

    }
}
