package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.UserDao;
import com.demowellstack.demowellstack.dao.UserSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Adminchecking {

    @Autowired
    UserSessionRepository userSessionRepository;
    @Autowired
    UserDao userDao;

    public boolean isAdminValid(String sessionId) {
        if (userDao.findByEmail(userSessionRepository.findByValue(sessionId).getEmail()).getType().equals("admin")) {
            return true;
        } else {
            return false;
        }
    }
}
