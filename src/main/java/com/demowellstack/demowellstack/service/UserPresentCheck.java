package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.UserSessionRepository;
import com.demowellstack.demowellstack.entity.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserPresentCheck {
    @Autowired
    UserSessionRepository userSessionRepository;

    public boolean isUserPresent(String email){
        if(userSessionRepository.findByEmailAndValidIsTrue(email).isPresent()){
            return true;
        }
        else {
            return false;
        }

    }
}
