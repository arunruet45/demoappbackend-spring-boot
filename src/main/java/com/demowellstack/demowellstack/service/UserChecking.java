package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserChecking {

    @Autowired
    UserSessionRepository userSessionRepository;

    public boolean isUserValid(String sessionId){
        if (userSessionRepository.findByValueAndValidIsTrue(sessionId).isPresent()){
            return true;
        }
        else {
            return false;
        }
    }
}
