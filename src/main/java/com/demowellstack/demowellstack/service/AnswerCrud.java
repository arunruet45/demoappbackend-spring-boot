package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.AnswerDao;
import com.demowellstack.demowellstack.dao.LikeDislikeDao;
import com.demowellstack.demowellstack.dao.QuestionDao;
import com.demowellstack.demowellstack.dao.UserDao;
import com.demowellstack.demowellstack.entity.Answer;
import com.demowellstack.demowellstack.entity.LikeDislike;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerCrud {

    @Autowired
    AnswerDao answerDao;

    @Autowired
    LikeDislikeDao likeDislikeDao;

    @Autowired
    UserDao userDao;

    @Autowired
    UserChecking userChecking;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    UserMatchChecking userMatchChecking;

    public void save(Answer answer, int id, String email) {
       answer.setQuestion(questionDao.findById(id).get());
       answer.setUser(userDao.findByEmail(email));
       answerDao.save(answer);
    }

    public void update(Answer answer, int id){
        Answer previousAnswer = answerDao.findById(id).get();
        previousAnswer.setDescription(answer.getDescription());
        answerDao.save(previousAnswer);
    }

    public void delete(int id) {
        Answer answer = answerDao.findById(id).get();
        answerDao.delete(answer);
//        Optional<List<LikeDislike>> likeDislikes =  likeDislikeDao.findByAnswer(answerDao.findById(id).get());
//        if (likeDislikes.isPresent()){
//            likeDislikeDao.delete((LikeDislike) likeDislikes.get());
//        }
    }
}
