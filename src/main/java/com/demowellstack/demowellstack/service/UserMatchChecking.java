package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.AnswerDao;
import com.demowellstack.demowellstack.dao.QuestionDao;
import com.demowellstack.demowellstack.dao.UserSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMatchChecking {
    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    AnswerDao answerDao;

    public boolean isQuestionUserMatched(int id, String sessionId){

        if (questionDao.findById(id).get().getUser().getEmail().equals(userSessionRepository.findByValue(sessionId).getEmail())){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isAnswerUserMatched(int id, String sessionId){

        if (answerDao.findById(id).get().getUser().getEmail().equals(userSessionRepository.findByValue(sessionId).getEmail())){
            return true;
        }
        else {
            return false;
        }
    }
}
