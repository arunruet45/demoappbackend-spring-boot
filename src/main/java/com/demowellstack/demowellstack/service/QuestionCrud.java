package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.AnswerDao;
import com.demowellstack.demowellstack.dao.CategoryDao;
import com.demowellstack.demowellstack.dao.QuestionDao;
import com.demowellstack.demowellstack.dao.UserDao;
import com.demowellstack.demowellstack.entity.Answer;
import com.demowellstack.demowellstack.entity.Question;
import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionCrud {

    @Autowired
    UserDao userDao;
    @Autowired
    QuestionDao questionDao;
    @Autowired
    AnswerDao answerDao;
    @Autowired
    CategoryDao categoryDao;

    @Autowired
    UserChecking userChecking;

    @Autowired
    UserMatchChecking userMatchChecking;

    public void save(Question question, int id, String email){
        question.setCategory(categoryDao.findById(id).get());
        question.setUser(userDao.findByEmail(email));
        questionDao.save(question);
    }

    public void update(Question question, int id, int cId){
        Question previousQuestion = questionDao.findById(id).get();
        previousQuestion.setTitle(question.getTitle());
        previousQuestion.setCategory(categoryDao.findById(cId).get());
        previousQuestion.setDescription(question.getDescription());
        questionDao.save(previousQuestion);
    }
    public void delete(int id){
        Question question = questionDao.findById(id).get();
        questionDao.delete(question);
//        Optional<List<Answer>> answers =  answerDao.findByQuestion(questionDao.findById(id).get());
//        if (answers.isPresent()){
//            answerDao.delete((Answer) answers.get());
//        }
    }
}
