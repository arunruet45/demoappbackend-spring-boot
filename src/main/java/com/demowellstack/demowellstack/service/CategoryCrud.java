package com.demowellstack.demowellstack.service;

import com.demowellstack.demowellstack.dao.CategoryDao;
import com.demowellstack.demowellstack.dao.QuestionDao;
import com.demowellstack.demowellstack.entity.Category;
import com.demowellstack.demowellstack.entity.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryCrud {

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    UserChecking userChecking;

    @Autowired
    Adminchecking adminchecking;

    public void save(Category category){
        categoryDao.save(category);
    }

    public void update(Category category, int id){
        Category previousCategory = categoryDao.findById(id).get();
        previousCategory.setType(category.getType());
        categoryDao.save(previousCategory);
    }
    public void delete(int id){
//        Optional<List<Question>> questions =  questionDao.findByCategory(categoryDao.findById(id).get());
//        if (questions.isPresent()){
//            questionDao.delete((Question) questions.get());
//        }
        questionDao.deleteByCategory(categoryDao.findById(id).get());
        Category category = categoryDao.findById(id).get();
        categoryDao.delete(category);
    }

    public List<Category> findAll(){
        List<Category> categories;
        categories = categoryDao.findAll();
        return categories;
    }
}
