package com.demowellstack.demowellstack.service;

import org.springframework.http.ResponseEntity;

interface GeneralCrud {

    ResponseEntity<Object> save(Object object);
    ResponseEntity<Object> update(Object object);
    ResponseEntity<Object> delete(int id);
    ResponseEntity<Object> findOne(int id);
    ResponseEntity<Object> findAll();
}
