package com.demowellstack.demowellstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemowellstackApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemowellstackApplication.class, args);
        System.out.println("Spring Boot initialized");
    }

}
